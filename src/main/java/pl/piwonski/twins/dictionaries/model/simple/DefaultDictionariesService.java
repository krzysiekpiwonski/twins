package pl.piwonski.twins.dictionaries.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.model.DictionariesService;
import pl.piwonski.twins.dictionaries.persist.DictionariesDao;
import pl.piwonski.twins.events.domain.EventType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * @author krzysztof.piwonski@agora.pl
 */
@Stateless
public class DefaultDictionariesService implements DictionariesService {
    @Inject
    @PreferredImplementation
    private DictionariesDao dictionariesDao;

    @Override
    public List<DictionaryEntry> getStatuses(final EventType eventType) {
        final DictionaryName dictionaryName;
        switch (eventType) {
            case BUG:
                dictionaryName = DictionaryName.STATUS_BUG;
                break;
            case CHANGE:
                dictionaryName = DictionaryName.STATUS_CHANGE;
                break;
            default:
                dictionaryName = DictionaryName.STATUS_INCIDENT;
        }
        return dictionariesDao.getDictionaryEntries(dictionaryName);
    }

    @Override
    public List<DictionaryEntry> getPriorities() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.PRIORITY);
    }

    @Override
    public List<DictionaryEntry> getOperatingSystems() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.OPERATING_SYSTEM);
    }

    @Override
    public List<DictionaryEntry> getProjects() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.PROJECT_NAME);
    }

    @Override
    public List<DictionaryEntry> getProjectModules() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.PROJECT_MODULE);
    }

    @Override
    public List<DictionaryEntry> getProjectVersions() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.PROJECT_VERSION);
    }

    @Override
    public List<DictionaryEntry> getBrowsers() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.BROWSER);
    }

    @Override
    public List<DictionaryEntry> getAreas() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.AREA);
    }

    @Override
    public List<DictionaryEntry> getAcceptances() {
        return dictionariesDao.getDictionaryEntries(DictionaryName.ACCEPTANCE);
    }
}
