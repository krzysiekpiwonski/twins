package pl.piwonski.twins.dictionaries.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.model.DictionariesEditionService;
import pl.piwonski.twins.dictionaries.persist.DictionariesDao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * @author krzysztof.piwonski@agora.pl
 */
@Stateless
public class DefaultDictionariesEditionService implements DictionariesEditionService {
    @Inject
    @PreferredImplementation
    private DictionariesDao dictionariesDao;

    @Override
    public Map<DictionaryName, List<DictionaryEntry>> exportEntries() {
        return dictionariesDao.getDictionaryEntries();
    }

    @Override
    public void importEntries(final Map<DictionaryName, List<DictionaryEntry>> entries) {
        dictionariesDao.updateDictionaryEntries(entries);
    }
}
