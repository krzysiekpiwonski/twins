package pl.piwonski.twins.dictionaries.model;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;

import java.util.List;
import java.util.Map;

/**
 * @author krzysiek@piwonski.pl
 */
public interface DictionariesEditionService {
    Map<DictionaryName, List<DictionaryEntry>> exportEntries();

    void importEntries(Map<DictionaryName, List<DictionaryEntry>> entries);
}
