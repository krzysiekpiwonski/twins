package pl.piwonski.twins.dictionaries.model;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.events.domain.EventType;

import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public interface DictionariesService {
    List<DictionaryEntry> getStatuses(EventType eventType);

    List<DictionaryEntry> getPriorities();

    List<DictionaryEntry> getOperatingSystems();

    List<DictionaryEntry> getProjects();

    List<DictionaryEntry> getProjectModules();

    List<DictionaryEntry> getProjectVersions();

    List<DictionaryEntry> getBrowsers();

    List<DictionaryEntry> getAreas();

    List<DictionaryEntry> getAcceptances();
}
