package pl.piwonski.twins.dictionaries.persist.binaryfile;

import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.persist.DictionariesDao;
import pl.piwonski.twins.dictionaries.persist.comparator.CompareDictionaryEntriesByValueToDisplay;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krzysiek@piwonski.pl
 */
final class DefaultDictionariesDao implements DictionariesDao {
    private static final Logger LOGGER = Logger.getLogger(DefaultDictionariesDao.class.getSimpleName());
    private final FileManager fileManager;
    private final File file;

    DefaultDictionariesDao(final FileManager fileManager, final File file) {
        this.fileManager = fileManager;
        this.file = file;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<DictionaryName, List<DictionaryEntry>> getDictionaryEntries() {
        EnumMap<DictionaryName, List<DictionaryEntry>> dictionaries;
        try {
            dictionaries = (EnumMap<DictionaryName, List<DictionaryEntry>>) fileManager.readObject(file);
            if (dictionaries == null) {
                dictionaries = new EnumMap<DictionaryName, List<DictionaryEntry>>(DictionaryName.class);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting dictionaries:", e);
            throw new DaoException(e);
        }
        return dictionaries;
    }

    @Override
    public List<DictionaryEntry> getDictionaryEntries(final DictionaryName dictionaryName) {
        final List<DictionaryEntry> dictionaryEntries = getDictionaryEntries().get(dictionaryName);
        Collections.sort(dictionaryEntries, new CompareDictionaryEntriesByValueToDisplay());
        return dictionaryEntries;
    }

    @Override
    public void updateDictionaryEntries(final Map<DictionaryName, List<DictionaryEntry>> entries) {
        try {
            fileManager.writeObject(file, (Serializable) entries);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "ERROR when updating dictionaries:", e);
            throw new DaoException(e);
        }
    }
}
