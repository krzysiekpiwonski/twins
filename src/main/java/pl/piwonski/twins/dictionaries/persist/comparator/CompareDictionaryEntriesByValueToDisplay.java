package pl.piwonski.twins.dictionaries.persist.comparator;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class CompareDictionaryEntriesByValueToDisplay implements Comparator<DictionaryEntry>,Serializable {
    private static final long serialVersionUID = 4758028423435799380L;

    @Override
    public int compare(final DictionaryEntry o1, final DictionaryEntry o2) {
        return o1.valueToDisplay.compareTo(o2.valueToDisplay);
    }
}
