package pl.piwonski.twins.dictionaries.persist;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;

import java.util.List;
import java.util.Map;

/**
 * @author krzysiek@piwonski.pl
 */
public interface DictionariesDao {
    Map<DictionaryName, List<DictionaryEntry>> getDictionaryEntries();

    List<DictionaryEntry> getDictionaryEntries(DictionaryName dictionaryName);

    void updateDictionaryEntries(Map<DictionaryName, List<DictionaryEntry>> entries);
}
