package pl.piwonski.twins.dictionaries.persist.binaryfile;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.dictionaries.persist.DictionariesDao;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.File;

/**
 * @author krzysiek@piwonski.pl
 */
@ApplicationScoped
public class DaoFactory {
    @Inject
    private MessageHelper messagesHelper;

    @Produces
    @PreferredImplementation
    @ApplicationScoped
    public DictionariesDao createEventDictionaryDao() {
        return new DefaultDictionariesDao(createFileManager(), getFile(MessageKey.CONFIG_DICTIONARIES_DB_FILE));
    }

    @Produces
    private FileManager createFileManager() {
        return new FileManager();
    }

    private File getFile(final MessageKey key) {
        return new File(messagesHelper.getMessage(key));
    }
}
