package pl.piwonski.twins.dictionaries.domain;

import java.io.Serializable;

/**
 * @author krzysiek@piwonski.pl
 */
public final class DictionaryEntry implements Serializable {
    private static final long serialVersionUID = 2504099345063766811L;
    public final String value;
    public final String valueToDisplay;

    public DictionaryEntry(final String value, final String valueToDisplay) {
        this.value = value;
        this.valueToDisplay = valueToDisplay;
    }
}
