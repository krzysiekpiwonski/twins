package pl.piwonski.twins.dictionaries.domain;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public enum DictionaryName {
    STATUS_CHANGE,
    STATUS_INCIDENT,
    PRIORITY,
    ACCEPTANCE,
    OPERATING_SYSTEM,
    PROJECT_NAME,
    PROJECT_MODULE,
    PROJECT_VERSION,
    BROWSER,
    AREA,
    STATUS_BUG
}
