package pl.piwonski.twins.dictionaries.controller;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.model.DictionariesEditionService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krzysiek@piwonski.pl
 */
@WebServlet(urlPatterns = "/admin/dictionaries/export")
public class ExportDictionaries extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ExportDictionaries.class.getSimpleName());
    @Inject
    private DictionariesEditionService dictionariesService;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/csv");
            final Map<DictionaryName, List<DictionaryEntry>> dictionaryEntries = dictionariesService.exportEntries();
            for (final Map.Entry<DictionaryName, List<DictionaryEntry>> entry : dictionaryEntries.entrySet()) {
                for (final DictionaryEntry dictionaryEntry : entry.getValue()) {
                    response.getWriter().write(buildLine(entry.getKey(), dictionaryEntry));
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "ERROR when exporting dictionaries:", e);
        }
    }

    private String buildLine(final DictionaryName dictionaryName, final DictionaryEntry dictionaryEntry) {
        return dictionaryName + ";" + dictionaryEntry.value + ";" + dictionaryEntry.valueToDisplay + '\n';
    }
}
