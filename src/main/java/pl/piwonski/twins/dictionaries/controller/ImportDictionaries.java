package pl.piwonski.twins.dictionaries.controller;

import org.apache.myfaces.custom.fileupload.UploadedFile;
import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.model.DictionariesEditionService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class ImportDictionaries {
    private static final Logger LOGGER = Logger.getLogger(ImportDictionaries.class.getSimpleName());
    private static final Pattern LINE_SPLITTER = Pattern.compile(";");
    @Inject
    private DictionariesEditionService dictionariesService;

    private UploadedFile file;

    public String uploadFile() {
        try {
            if (file != null) {
                final Map<DictionaryName, List<DictionaryEntry>> entries
                        = new EnumMap<DictionaryName, List<DictionaryEntry>>(DictionaryName.class);
                final BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
                String line = reader.readLine();
                while (line != null) {
                    final String[] entryParts = LINE_SPLITTER.split(line);
                    if (!entries.containsKey(DictionaryName.valueOf(entryParts[0]))) {
                        entries.put(DictionaryName.valueOf(entryParts[0]), new ArrayList<DictionaryEntry>());
                    }
                    entries.get(DictionaryName.valueOf(entryParts[0])).add(new DictionaryEntry(entryParts[1],
                            entryParts[2]));
                    line = reader.readLine();
                }
                reader.close();
                dictionariesService.importEntries(entries);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "ERROR when importing dictionaries:", e);
        }
        return "";
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(final UploadedFile file) {
        this.file = file;
    }
}
