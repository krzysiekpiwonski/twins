package pl.piwonski.twins.users.persist.comparator;

import pl.piwonski.twins.users.domain.User;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class CompareUsersBySurname implements Comparator<User>,Serializable {
    private static final long serialVersionUID = 4211849008264239521L;

    @Override
    public int compare(final User o1, final User o2) {
        return o1.getSurname().toUpperCase().compareTo(o2.getSurname().toUpperCase());
    }
}
