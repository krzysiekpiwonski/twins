package pl.piwonski.twins.users.persist.binaryfile;

import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.persist.UserDao;
import pl.piwonski.twins.users.persist.comparator.CompareUsersBySurname;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krzysiek@piwonski.pl
 */
final class DefaultUserDao implements UserDao {
    private static final Logger LOGGER = Logger.getLogger(DefaultUserDao.class.getSimpleName());
    private final FileManager fileManager;
    private final File file;

    DefaultUserDao(final FileManager fileManager, final File file) {
        this.fileManager = fileManager;
        this.file = file;
    }

    @Override
    public void saveUser(final User user) {
        try {
            final HashMap<Long, User> users = readUsers();
            users.put(user.getId(), user);
            fileManager.writeObject(file, users);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when saving user:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public void deleteUser(final long id) {
        try {
            final HashMap<Long, User> users = readUsers();
            users.remove(id);
            fileManager.writeObject(file, users);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when removing user:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public User getUser(final long id) {
        final User user;
        try {
            user = readUsers().get(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting user by ID:", e);
            throw new DaoException(e);
        }
        return user;
    }

    @Override
    public User getUser(final String login) {
        final User user;
        try {
            user = getUserByLogin(readUsers(), login);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting user by login:", e);
            throw new DaoException(e);
        }
        return user;
    }

    private User getUserByLogin(final Map<Long, User> users, final String login) {
        User resultUser = null;
        for (final User user : users.values()) {
            if (login.equals(user.getLogin())) {
                resultUser = user;
            }
        }
        return resultUser;
    }

    @Override
    public List<User> getUsers() {
        final List<User> users;
        try {
            users = new ArrayList<User>(readUsers().values());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting users:", e);
            throw new DaoException(e);
        }
        Collections.sort(users, new CompareUsersBySurname());
        return users;
    }

    @SuppressWarnings("unchecked")
    private HashMap<Long, User> readUsers() throws ClassNotFoundException, IOException {
        HashMap<Long, User> users = (HashMap<Long, User>) fileManager.readObject(file);
        if (users == null) {
            users = new HashMap<Long, User>();
        }
        return users;
    }
}
