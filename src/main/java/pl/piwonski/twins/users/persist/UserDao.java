package pl.piwonski.twins.users.persist;

import pl.piwonski.twins.users.domain.User;

import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public interface UserDao {
    void saveUser(User user);

    void deleteUser(long id);

    User getUser(long id);

    User getUser(String login);

    List<User> getUsers();
}
