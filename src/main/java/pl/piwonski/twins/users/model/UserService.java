package pl.piwonski.twins.users.model;

import pl.piwonski.twins.users.domain.User;

import java.util.List;
import java.util.Map;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public interface UserService {
    void addUser(User user);

    void updateUser(User user);

    void deleteUser(long id);

    User getUser(long id);

    User getUser(String login);

    List<User> getUsers();

    Map<String, List<User>> getUsersDividedBySurnameFirstLetter();
}
