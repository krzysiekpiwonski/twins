package pl.piwonski.twins.users.model.simple;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public class LoginAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = -2996948545634781966L;
}
