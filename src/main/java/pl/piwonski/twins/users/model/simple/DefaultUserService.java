package pl.piwonski.twins.users.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.model.UserService;
import pl.piwonski.twins.users.persist.UserDao;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author krzysiek@piwonski.pl
 */
public class DefaultUserService implements UserService {
    @Inject
    @PreferredImplementation
    private UserDao userDao;

    @Override
    public void addUser(final User user) {
        if (userDao.getUser(user.getLogin()) != null) {
            throw new LoginAlreadyExistsException();
        }
        userDao.saveUser(user);
    }

    @Override
    public void updateUser(final User user) {
        userDao.saveUser(user);
    }

    @Override
    public void deleteUser(final long id) {
        userDao.deleteUser(id);
    }

    @Override
    public User getUser(final long id) {
        return userDao.getUser(id);
    }

    @Override
    public User getUser(final String login) {
        return userDao.getUser(login);
    }

    @Override
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @Override
    public Map<String, List<User>> getUsersDividedBySurnameFirstLetter() {
        final Map<String, List<User>> dividedUsers = new LinkedHashMap<String, List<User>>();
        for (final User user : userDao.getUsers()) {
            final String userSurnameFirstLetter = user.getSurname().toUpperCase().substring(0, 1);
            if (!dividedUsers.containsKey(userSurnameFirstLetter)) {
                dividedUsers.put(userSurnameFirstLetter, new ArrayList<User>());
            }
            dividedUsers.get(userSurnameFirstLetter).add(user);
        }
        return dividedUsers;
    }
}
