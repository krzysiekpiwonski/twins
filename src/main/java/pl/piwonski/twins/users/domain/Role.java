package pl.piwonski.twins.users.domain;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public enum Role {
    ADMIN("administrator"),
    DEVELOPER("programista"),
    TESTER("tester"),
    CEO("kierownik"),
    PM("project manager");

    private final String nameToDisplay;

    Role(final String nameToDisplay) {
        this.nameToDisplay = nameToDisplay;
    }

    public String getNameToDisplay() {
        return nameToDisplay;
    }
}
