package pl.piwonski.twins.users.domain;

import pl.piwonski.twins.common.IdSequence;
import pl.piwonski.twins.common.TwinsPasswordEncoder;
import pl.piwonski.twins.common.domain.validator.Id;
import pl.piwonski.twins.common.domain.validator.Login;
import pl.piwonski.twins.common.domain.validator.Text;
import pl.piwonski.twins.users.domain.validator.*;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author krzysiek@piwonski.pl
 */
public class User implements Serializable {
    private static final long serialVersionUID = -4142440066911592531L;
    private static final String TEXT_EMPTY_FIELD = "{empty.field}";

    @Id
    @NotNull(message = TEXT_EMPTY_FIELD)
    private long id;

    @Login
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String login;

    @Text
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String name;

    @Text
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String surname;

    @Email
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String email;

    @Phone
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String phone;

    @Password
    private String password;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private Set<Role> roles;

    @Inject
    private IdSequence idSequence;

    public User() {
        this.id = idSequence.getNextValue();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return (roles == null) ? Collections.<Role>emptySet() : Collections.unmodifiableSet(roles);
    }

    public String getDisplayName() {
        return name + ' ' + surname;
    }

    public boolean isAdmin() {
        return roles.contains(Role.ADMIN);
    }

    public boolean isDeveloper() {
        return roles.contains(Role.DEVELOPER);
    }

    public boolean isTester() {
        return roles.contains(Role.TESTER);
    }

    public boolean isPm() {
        return roles.contains(Role.PM);
    }

    public boolean isCeo() {
        return roles.contains(Role.CEO);
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public void setPassword(final String password) {
        this.password = TwinsPasswordEncoder.encodePassword(password);
    }

    public void setRoles(final Set<Role> roles) {
        this.roles = new HashSet<Role>(roles);
    }
}
