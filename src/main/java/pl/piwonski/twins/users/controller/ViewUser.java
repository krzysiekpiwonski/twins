package pl.piwonski.twins.users.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.model.UserService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@ManagedBean
@ViewScoped
public class ViewUser {
    @Inject
    private UserService userService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;

    private User user;

    public User getUser() {
        if (user == null) {
            user = userService.getUser(requestParameters.getLongValue("id"));
        }
        return user;
    }

    public String saveUser() {
        String destination;
        try {
            userService.updateUser(user);
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_EDIT);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_NOT_SAVED);
        }
        return destination;
    }
}
