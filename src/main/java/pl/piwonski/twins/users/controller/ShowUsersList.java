package pl.piwonski.twins.users.controller;

import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.model.UserService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class ShowUsersList {
    @Inject
    private UserService userService;

    public Map<String, List<User>> getUsers() {
        return userService.getUsersDividedBySurnameFirstLetter();
    }
}
