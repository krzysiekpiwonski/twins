package pl.piwonski.twins.users.controller;

import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.model.UserService;
import pl.piwonski.twins.users.model.simple.LoginAlreadyExistsException;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class AddUser {
    @Inject
    private UserService userService;
    @Inject
    private MessageHelper messagesHelper;

    @Named
    @Produces
    private final User user = new User();

    public String add() {
        String destination;
        try {
            userService.addUser(user);
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_SAVED);
        } catch (final LoginAlreadyExistsException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_NOT_SAVED_EXISTING_LOGIN);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_NOT_SAVED);
        }
        return destination;
    }
}
