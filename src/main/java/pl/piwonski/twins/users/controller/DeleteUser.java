package pl.piwonski.twins.users.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.users.model.UserService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class DeleteUser {
    @Inject
    private UserService userService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;

    public String delete() {
        String destination;
        try {
            userService.deleteUser(requestParameters.getLongValue("id"));
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_DELETED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_USERS_SHOW);
            messagesHelper.addMessageToPage(MessageKey.TEXT_USERS_NOT_DELETED);
        }
        return destination;
    }
}
