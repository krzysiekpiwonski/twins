package pl.piwonski.twins.events.domain;

import javax.validation.constraints.NotNull;

/**
 * @author krzysiek@piwonski.pl
 */
public final class Bug extends AbstractEvent {
    private static final long serialVersionUID = 1418279124731600357L;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String browser;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String operatingSystem;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String projectVersion;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String projectModule;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String project;

    public Bug() {
        browser = "";
        operatingSystem = "";
        projectVersion = "";
        projectModule = "";
        project = "";
    }

    public String getBrowser() {
        return browser;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public String getProjectVersion() {
        return projectVersion;
    }

    public String getProjectModule() {
        return projectModule;
    }

    public String getProject() {
        return project;
    }

    public void setBrowser(final String browser) {
        this.browser = browser;
    }

    public void setOperatingSystem(final String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public void setProjectVersion(final String projectVersion) {
        this.projectVersion = projectVersion;
    }

    public void setProjectModule(final String projectModule) {
        this.projectModule = projectModule;
    }

    public void setProject(final String project) {
        this.project = project;
    }
}
