package pl.piwonski.twins.events.domain;

import pl.piwonski.twins.common.IdSequence;
import pl.piwonski.twins.common.domain.validator.Id;
import pl.piwonski.twins.common.domain.validator.Login;
import pl.piwonski.twins.common.domain.validator.Text;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

/**
 * @author krzysiek@piwonski.pl
 */
public abstract class AbstractEvent implements Serializable {
    private static final long serialVersionUID = 7139596781977677488L;
    protected static final String TEXT_EMPTY_FIELD = "{empty.field}";

    @Id
    @NotNull(message = TEXT_EMPTY_FIELD)
    private long id;

    @Login
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String applicant;

    @Login
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String responsible;

    @Past
    @NotNull(message = TEXT_EMPTY_FIELD)
    private Date openingDate;

    @Text
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String status;

    @Text
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String priority;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String title;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String description;

    protected AbstractEvent() {
        id = IdSequence.getNextValue();
        applicant = "";
        responsible = "";
        openingDate = new Date();
        status = "";
        priority = "";
        title = "";
        description = "";
    }

    public final long getId() {
        return id;
    }

    public final String getApplicant() {
        return applicant;
    }

    public final String getResponsible() {
        return responsible;
    }

    public final Date getOpeningDate() {
        return (Date) openingDate.clone();
    }

    public final String getStatus() {
        return status;
    }

    public final String getPriority() {
        return priority;
    }

    public final String getTitle() {
        return title;
    }

    public final String getDescription() {
        return description;
    }

    public final void setId(final long id) {
        this.id = id;
    }

    public final void setApplicant(final String applicant) {
        this.applicant = applicant;
    }

    public final void setResponsible(final String responsible) {
        this.responsible = responsible;
    }

    public final void setTitle(final String title) {
        this.title = title;
    }

    public final void setOpeningDate(final Date openingDate) {
        this.openingDate = (Date) openingDate.clone();
    }

    public final void setStatus(final String status) {
        this.status = status;
    }

    public final void setDescription(final String description) {
        this.description = description;
    }

    public final void setPriority(final String priority) {
        this.priority = priority;
    }
}
