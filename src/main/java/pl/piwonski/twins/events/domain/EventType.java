package pl.piwonski.twins.events.domain;

/**
 * @author krzysiek@piwonski.pl
 */
public enum EventType {
    BUG,
    CHANGE,
    INCIDENT
}
