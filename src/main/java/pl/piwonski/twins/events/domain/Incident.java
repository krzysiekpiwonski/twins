package pl.piwonski.twins.events.domain;

import javax.validation.constraints.NotNull;

/**
 * @author krzysiek@piwonski.pl
 */
public final class Incident extends AbstractEvent {
    private static final long serialVersionUID = -6105522585910427752L;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String area;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String symptoms;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String cause;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String wayOfRepair;

    public Incident() {
        area = "";
        symptoms = "";
        cause = "";
        wayOfRepair = "";
    }

    public String getArea() {
        return area;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public String getCause() {
        return cause;
    }

    public String getWayOfRepair() {
        return wayOfRepair;
    }

    public void setArea(final String area) {
        this.area = area;
    }

    public void setSymptoms(final String symptoms) {
        this.symptoms = symptoms;
    }

    public void setCause(final String cause) {
        this.cause = cause;
    }

    public void setWayOfRepair(final String wayOfRepair) {
        this.wayOfRepair = wayOfRepair;
    }
}
