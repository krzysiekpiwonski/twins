package pl.piwonski.twins.events.domain;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author krzysiek@piwonski.pl
 */
public final class Change extends AbstractEvent {
    private static final long serialVersionUID = 7998827757506666812L;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String acceptance;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String cancelDescription;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String riskDescription;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private Date startDate;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private Date endDate;

    public Change() {
        startDate = new Date();
        endDate = new Date();
        acceptance = "";
        cancelDescription = "";
        riskDescription = "";
    }

    public String getAcceptance() {
        return acceptance;
    }

    public String getCancelDescription() {
        return cancelDescription;
    }

    public String getRiskDescription() {
        return riskDescription;
    }

    public Date getStartDate() {
        return (Date) startDate.clone();
    }

    public Date getEndDate() {
        return (Date) endDate.clone();
    }

    public void setAcceptance(final String acceptance) {
        this.acceptance = acceptance;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = (Date) startDate.clone();
    }

    public void setEndDate(final Date endDate) {
        this.endDate = (Date) endDate.clone();
    }

    public void setCancelDescription(final String cancelDescription) {
        this.cancelDescription = cancelDescription;
    }

    public void setRiskDescription(final String riskDescription) {
        this.riskDescription = riskDescription;
    }
}
