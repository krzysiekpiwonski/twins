package pl.piwonski.twins.events.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.persist.EventDao;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Stateless
public class BugService extends AbstractEventService<Bug> {
    @Inject
    @PreferredImplementation
    private EventDao<Bug> bugDao;

    @Override
    protected EventDao<Bug> getDao() {
        return bugDao;
    }
}
