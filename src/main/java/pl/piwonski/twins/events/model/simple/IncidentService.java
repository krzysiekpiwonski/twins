package pl.piwonski.twins.events.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.events.domain.Incident;
import pl.piwonski.twins.events.persist.EventDao;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Stateless
public class IncidentService extends AbstractEventService<Incident> {
    @Inject
    @PreferredImplementation
    private EventDao<Incident> incidentDao;

    @Override
    protected EventDao<Incident> getDao() {
        return incidentDao;
    }
}
