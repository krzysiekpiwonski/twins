package pl.piwonski.twins.events.model.simple;

import pl.piwonski.twins.events.domain.AbstractEvent;
import pl.piwonski.twins.events.model.EventService;
import pl.piwonski.twins.events.persist.EventDao;

import java.util.Collections;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public abstract class AbstractEventService<T extends AbstractEvent> implements EventService<T> {
    @Override
    public void addEvent(final T event) {
        getDao().saveEvent(event);
    }

    @Override
    public void updateEvent(final T event) {
        getDao().saveEvent(event);
    }

    @Override
    public void deleteEvent(final long id) {
        getDao().deleteEvent(id);
    }

    @Override
    public T getEvent(final long id) {
        return getDao().getEvent(id);
    }

    @Override
    public List<T> getEvents() {
        return Collections.unmodifiableList(getDao().getEvents());
    }

    @Override
    public List<T> getEvents(final int maximumElements) {
        List<T> events = getDao().getEvents();
        if (events.size() > maximumElements) {
            events = events.subList(0, maximumElements);
        }
        return Collections.unmodifiableList(events);
    }

    protected abstract EventDao<T> getDao();
}
