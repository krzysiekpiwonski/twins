package pl.piwonski.twins.events.model;

import pl.piwonski.twins.events.domain.AbstractEvent;

import java.util.List;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public interface EventService<T extends AbstractEvent> {
    void addEvent(T event);

    void updateEvent(T event);

    void deleteEvent(long id);

    T getEvent(long id);

    List<T> getEvents();

    List<T> getEvents(int maximumElements);
}
