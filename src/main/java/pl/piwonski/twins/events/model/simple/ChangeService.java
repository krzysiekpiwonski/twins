package pl.piwonski.twins.events.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.persist.EventDao;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Stateless
public class ChangeService extends AbstractEventService<Change> {
    @Inject
    @PreferredImplementation
    private EventDao<Change> changeDao;

    @Override
    protected EventDao<Change> getDao() {
        return changeDao;
    }
}
