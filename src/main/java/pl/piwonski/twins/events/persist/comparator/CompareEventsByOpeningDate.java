package pl.piwonski.twins.events.persist.comparator;

import pl.piwonski.twins.events.domain.AbstractEvent;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class CompareEventsByOpeningDate implements Comparator<AbstractEvent>, Serializable {
    private static final long serialVersionUID = -5332012915890919957L;

    @Override
    public int compare(final AbstractEvent o1, final AbstractEvent o2) {
        return o2.getOpeningDate().compareTo(o1.getOpeningDate());
    }
}
