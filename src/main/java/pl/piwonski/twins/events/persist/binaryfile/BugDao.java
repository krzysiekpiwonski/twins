package pl.piwonski.twins.events.persist.binaryfile;

import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.events.domain.Bug;

import java.io.File;

/**
 * @author krzysiek@piwonski.pl
 */
final class BugDao extends AbstractEventDao<Bug> {
    BugDao(final FileManager fileManager, final File file) {
        super(fileManager, file);
    }
}
