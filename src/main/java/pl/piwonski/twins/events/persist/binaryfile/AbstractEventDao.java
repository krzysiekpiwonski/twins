package pl.piwonski.twins.events.persist.binaryfile;

import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.events.domain.AbstractEvent;
import pl.piwonski.twins.events.persist.EventDao;
import pl.piwonski.twins.events.persist.comparator.CompareEventsByOpeningDate;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krzysiek@piwonski.pl
 */
abstract class AbstractEventDao<T extends AbstractEvent> implements EventDao<T> {
    private static final Logger LOGGER = Logger.getLogger(AbstractEventDao.class.getSimpleName());
    private final FileManager fileManager;
    private final File file;

    AbstractEventDao(final FileManager fileManager, final File file) {
        this.fileManager = fileManager;
        this.file = file;
    }

    @Override
    public final void saveEvent(final T event) {
        try {
            final HashMap<Long, T> events = readEvents();
            events.put(event.getId(), event);
            fileManager.writeObject(file, events);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when saving event:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public final void deleteEvent(final long id) {
        try {
            final HashMap<Long, T> events = readEvents();
            events.remove(id);
            fileManager.writeObject(file, events);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when removing event:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public final T getEvent(final long id) {
        final T event;
        try {
            event = readEvents().get(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting event:", e);
            throw new DaoException(e);
        }
        return event;
    }

    @Override
    public final List<T> getEvents() {
        final Collection<T> events;
        try {
            events = readEvents().values();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting events:", e);
            throw new DaoException(e);
        }
        final List<T> eventsToSort = new ArrayList<T>(events);
        Collections.sort(eventsToSort, new CompareEventsByOpeningDate());
        return eventsToSort;
    }

    @SuppressWarnings("unchecked")
    private HashMap<Long, T> readEvents() throws ClassNotFoundException, IOException {
        HashMap<Long, T> events = (HashMap<Long, T>) fileManager.readObject(file);
        if (events == null) {
            events = new HashMap<Long, T>();
        }
        return events;
    }
}
