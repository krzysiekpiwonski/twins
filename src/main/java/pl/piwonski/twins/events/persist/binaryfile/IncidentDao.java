package pl.piwonski.twins.events.persist.binaryfile;

import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.events.domain.Incident;

import java.io.File;

/**
 * @author krzysiek@piwonski.pl
 */
final class IncidentDao extends AbstractEventDao<Incident> {
    IncidentDao(final FileManager fileManager, final File file) {
        super(fileManager, file);
    }
}
