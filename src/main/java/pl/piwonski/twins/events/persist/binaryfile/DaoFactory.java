package pl.piwonski.twins.events.persist.binaryfile;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.domain.Incident;
import pl.piwonski.twins.events.persist.EventDao;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.File;

/**
 * @author krzysiek@piwonski.pl
 */
@ApplicationScoped
public class DaoFactory {
    @Inject
    private MessageHelper messagesHelper;

    @Produces
    @PreferredImplementation
    @ApplicationScoped
    public EventDao<Bug> createBugDao() {
        return new BugDao(createFileManager(), getFile(MessageKey.CONFIG_EVENTS_BUG_DB_FILE));
    }

    @Produces
    @PreferredImplementation
    @ApplicationScoped
    public EventDao<Change> createChangeDao() {
        return new ChangeDao(createFileManager(), getFile(MessageKey.CONFIG_EVENTS_CHANGE_DB_FILE));
    }

    @Produces
    @PreferredImplementation
    @ApplicationScoped
    public EventDao<Incident> createIncidentDao() {
        return new IncidentDao(createFileManager(), getFile(MessageKey.CONFIG_EVENTS_INCIDENT_DB_FILE));
    }

    @Produces
    private FileManager createFileManager() {
        return new FileManager();
    }

    private File getFile(final MessageKey key) {
        return new File(messagesHelper.getMessage(key));
    }
}
