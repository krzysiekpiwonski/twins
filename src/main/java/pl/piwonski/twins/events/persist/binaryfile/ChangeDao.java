package pl.piwonski.twins.events.persist.binaryfile;

import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.events.domain.Change;

import java.io.File;

/**
 * @author krzysiek@piwonski.pl
 */
final class ChangeDao extends AbstractEventDao<Change> {
    ChangeDao(final FileManager fileManager, final File file) {
        super(fileManager, file);
    }
}
