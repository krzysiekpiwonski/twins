package pl.piwonski.twins.events.persist;

import pl.piwonski.twins.events.domain.AbstractEvent;

import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public interface EventDao<T extends AbstractEvent> {
    void saveEvent(T event);

    void deleteEvent(long id);

    T getEvent(long id);

    List<T> getEvents();
}
