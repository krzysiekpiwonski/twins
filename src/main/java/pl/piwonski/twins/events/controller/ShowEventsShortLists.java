package pl.piwonski.twins.events.controller;

import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.domain.Incident;
import pl.piwonski.twins.events.model.EventService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class ShowEventsShortLists {
    private static final int MAXIMUM_ELEMENTS_NUMBER = 10;
    @Inject
    private EventService<Bug> bugService;
    @Inject
    private EventService<Change> changeService;
    @Inject
    private EventService<Incident> incidentService;

    public List<Bug> getBugs() {
        return bugService.getEvents(MAXIMUM_ELEMENTS_NUMBER);
    }

    public List<Change> getChanges() {
        return changeService.getEvents(MAXIMUM_ELEMENTS_NUMBER);
    }

    public List<Incident> getIncidents() {
        return incidentService.getEvents(MAXIMUM_ELEMENTS_NUMBER);
    }
}
