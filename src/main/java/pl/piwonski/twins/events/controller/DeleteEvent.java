package pl.piwonski.twins.events.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.domain.Incident;
import pl.piwonski.twins.events.model.EventService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class DeleteEvent {
    @Inject
    private EventService<Bug> bugService;
    @Inject
    private EventService<Change> changeService;
    @Inject
    private EventService<Incident> incidentService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;

    public String deleteBug() {
        String destination;
        try {
            bugService.deleteEvent(requestParameters.getLongValue("id"));
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_DELETED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_BUG_SHOW);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_NOT_DELETED);
        }
        return destination;
    }

    public String deleteChange() {
        String destination;
        try {
            changeService.deleteEvent(requestParameters.getLongValue("id"));
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_DELETED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_CHANGE_SHOW);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_NOT_DELETED);
        }
        return destination;
    }

    public String deleteIncident() {
        String destination;
        try {
            incidentService.deleteEvent(requestParameters.getLongValue("id"));
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_DELETED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INCIDENT_SHOW);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_NOT_DELETED);
        }
        return destination;
    }
}
