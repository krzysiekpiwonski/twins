package pl.piwonski.twins.events.controller;

import pl.piwonski.twins.common.IdSequence;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.events.model.EventService;
import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.domain.Incident;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class AddEvent {
    @Inject
    private EventService<Bug> bugService;
    @Inject
    private EventService<Change> changeService;
    @Inject
    private EventService<Incident> incidentService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private IdSequence idSequence;

    @Named
    @Produces
    private final Bug bug = new Bug();
    @Named
    @Produces
    private final Change change = new Change();
    @Named
    @Produces
    private final Incident incident = new Incident();

    public String addBug() {
        String destination;
        try {
            bugService.addEvent(bug);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_BUG_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_NOT_SAVED);
        }
        return destination;
    }

    public String addChange() {
        String destination;
        try {
            changeService.addEvent(change);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_CHANGE_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_NOT_SAVED);
        }
        return destination;
    }

    public String addIncident() {
        String destination;
        try {
            incidentService.addEvent(incident);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INCIDENT_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_NOT_SAVED);
        }
        return destination;
    }
}
