package pl.piwonski.twins.events.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.events.domain.Bug;
import pl.piwonski.twins.events.domain.Change;
import pl.piwonski.twins.events.domain.Incident;
import pl.piwonski.twins.events.model.EventService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@ManagedBean
@ViewScoped
public class ViewEvent {
    @Inject
    private EventService<Bug> bugService;
    @Inject
    private EventService<Change> changeService;
    @Inject
    private EventService<Incident> incidentService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;

    private Bug bug;
    private Change change;
    private Incident incident;

    public Bug getBug() {
        if (bug == null) {
            bug = bugService.getEvent(requestParameters.getLongValue("id"));
        }
        return bug;
    }

    public Change getChange() {
        if (change == null) {
            change = changeService.getEvent(requestParameters.getLongValue("id"));
        }
        return change;
    }

    public Incident getIncident() {
        if (incident == null) {
            incident = incidentService.getEvent(requestParameters.getLongValue("id"));
        }
        return incident;
    }

    public String saveBug() {
        String destination;
        try {
            bugService.updateEvent(bug);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_BUG_EDIT);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_BUG_NOT_SAVED);
        }
        return destination;
    }

    public String saveChange() {
        String destination;
        try {
            changeService.updateEvent(change);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_CHANGE_EDIT);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_CHANGE_NOT_SAVED);
        }
        return destination;
    }

    public String saveIncident() {
        String destination;
        try {
            incidentService.updateEvent(incident);
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_EVENTS_INCIDENT_EDIT);
            messagesHelper.addMessageToPage(MessageKey.TEXT_EVENTS_INCIDENT_NOT_SAVED);
        }
        return destination;
    }
}
