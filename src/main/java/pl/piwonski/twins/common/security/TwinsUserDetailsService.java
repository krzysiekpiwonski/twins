package pl.piwonski.twins.common.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.persist.UserDao;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class TwinsUserDetailsService implements UserDetailsService {
    private final UserDao userDao;

    public TwinsUserDetailsService(final UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final User user = userDao.getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return new TwinsUserDetails(user);
    }
}
