package pl.piwonski.twins.common.security;

import org.springframework.security.core.GrantedAuthority;
import pl.piwonski.twins.users.domain.Role;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class TwinsGrantedAuthority implements GrantedAuthority {
    private static final long serialVersionUID = 570065526293084503L;
    private final Role role;

    public TwinsGrantedAuthority(final Role role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role.name();
    }
}
