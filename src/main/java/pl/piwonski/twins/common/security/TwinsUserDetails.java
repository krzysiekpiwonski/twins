package pl.piwonski.twins.common.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.piwonski.twins.users.domain.Role;
import pl.piwonski.twins.users.domain.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class TwinsUserDetails implements UserDetails {
    private static final long serialVersionUID = -1459312110128799512L;
    private final User user;

    public TwinsUserDetails(final User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final Collection<Role> roles = user.getRoles();
        return (roles == null) ? Collections.<GrantedAuthority>emptyList() : convertRolesToAuthorities(roles);
    }

    private Collection<? extends GrantedAuthority> convertRolesToAuthorities(final Iterable<Role> roles) {
        final Collection<TwinsGrantedAuthority> authorities = new ArrayList<TwinsGrantedAuthority>();
        for (final Role role : roles) {
            authorities.add(new TwinsGrantedAuthority(role));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
