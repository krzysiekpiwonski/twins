package pl.piwonski.twins.common.security;

import pl.piwonski.twins.users.domain.Role;

import javax.faces.convert.EnumConverter;
import javax.faces.convert.FacesConverter;

/**
 * @author krzysztof.piwonski@agora.pl
 */
@FacesConverter("roleEnumConverter")
public class RoleEnumConverter extends EnumConverter {
    public RoleEnumConverter() {
        super(Role.class);
    }
}
