package pl.piwonski.twins.common;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.Map;

/**
 * @author krzysiek@piwonski.pl
 */
@Named
@ApplicationScoped
public class RequestParameters {
    public Long getLongValue(final String parameterName) {
        return parseLong(getRequestParameter(parameterName));
    }

    private String getRequestParameter(final String name) {
        final ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        final Map<String, String> requestParameterMap = context.getRequestParameterMap();
        final String value = requestParameterMap.get(name);
        return (value == null) ? "" : value;
    }

    private Long parseLong(final String stringValue) {
        long number;
        try {
            number = Long.parseLong(stringValue);
        } catch (NumberFormatException e) {
            number = -1L;
        }
        return number;
    }
}
