package pl.piwonski.twins.common.persist.binaryfile;

import java.io.*;

/**
 * @author krzysiek@piwonski.pl
 */
public final class FileManager {
    public void writeObject(final File fileToWrite, final Serializable object) throws IOException {
        final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileToWrite));
        out.writeObject(object);
        out.close();
    }

    public Object readObject(final File fileToRead) throws ClassNotFoundException, IOException {
        Object object = null;
        if (fileToRead.length() > 0) {
            final ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileToRead));
            object = in.readObject();
            in.close();
        }
        return object;
    }
}
