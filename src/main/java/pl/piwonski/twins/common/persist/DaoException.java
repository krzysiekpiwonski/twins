package pl.piwonski.twins.common.persist;

/**
 * @author krzysiek@piwonski.pl
 */
public class DaoException extends RuntimeException {
    private static final long serialVersionUID = 9117510368530137033L;

    public DaoException(final Throwable cause) {
        super(cause);
    }
}
