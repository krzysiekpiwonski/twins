package pl.piwonski.twins.common.domain.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.*;

/**
 * @author krzysztof.piwonski@agora.pl
 */
@Pattern(regexp = "[A-Za-ząćęłńóśźżĄĆĘŁŃÓŚŹŻ]*", message = "{invalid.text}")
@Constraint(validatedBy = {})
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Text {
    String message() default "{invalid.text}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.FIELD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        Text[] value();
    }
}
