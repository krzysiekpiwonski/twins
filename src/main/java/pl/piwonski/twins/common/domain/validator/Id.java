package pl.piwonski.twins.common.domain.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.lang.annotation.*;

/**
 * @author krzysztof.piwonski@agora.pl
 */
@Min(value = Long.MIN_VALUE, message = "{invalid.id}")
@Max(value = Long.MAX_VALUE, message = "{invalid.id}")
@Constraint(validatedBy = {})
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {
    String message() default "{invalid.id}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.FIELD, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        Id[] value();
    }
}
