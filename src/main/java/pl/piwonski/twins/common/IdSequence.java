package pl.piwonski.twins.common;

import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;

import java.io.File;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public class IdSequence {
    private static final File FILE = new File(new MessageHelper().getMessage(MessageKey.CONFIG_ID_SEQUENCE_FILE));
    private static final FileManager FILE_MANAGER = new FileManager();

    private IdSequence() {
    }

    public static Long getNextValue() {
        Long nextValue;
        try {
            final Long currentValue = (Long) FILE_MANAGER.readObject(FILE);
            nextValue = currentValue + 1L;
            FILE_MANAGER.writeObject(FILE, nextValue);
        } catch (Exception e) {
            nextValue = 0L;
        }
        return nextValue;
    }
}
