package pl.piwonski.twins.common;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public class TwinsPasswordEncoder {
    private static final PasswordEncoder PASSWORD_ENCODER = new ShaPasswordEncoder();

    private TwinsPasswordEncoder() {
    }

    public static String encodePassword(final String password) {
        return PASSWORD_ENCODER.encodePassword(password, null);
    }
}
