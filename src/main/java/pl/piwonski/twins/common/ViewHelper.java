package pl.piwonski.twins.common;

import pl.piwonski.twins.dictionaries.domain.DictionaryEntry;
import pl.piwonski.twins.dictionaries.domain.DictionaryName;
import pl.piwonski.twins.dictionaries.model.DictionariesService;
import pl.piwonski.twins.events.domain.EventType;
import pl.piwonski.twins.users.domain.Role;
import pl.piwonski.twins.users.domain.User;
import pl.piwonski.twins.users.model.UserService;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
@Named
@ApplicationScoped
public class ViewHelper {
    @Inject
    private DictionariesService dictionariesService;
    @Inject
    private UserService userService;

    public User getLoggedUser() {
        return getUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
    }

    public User getUser(final String login) {
        return userService.getUser(login);
    }

    public List<SelectItem> getUsers() {
        return convertUsers(userService.getUsers());
    }

    public List<SelectItem> getStatusesForBug() {
        return convertDictionaryItems(dictionariesService.getStatuses(EventType.BUG));
    }

    public List<SelectItem> getStatusesForChange() {
        return convertDictionaryItems(dictionariesService.getStatuses(EventType.CHANGE));
    }

    public List<SelectItem> getStatusesForIncident() {
        return convertDictionaryItems(dictionariesService.getStatuses(EventType.INCIDENT));
    }

    public List<SelectItem> getPriorities() {
        return convertDictionaryItems(dictionariesService.getPriorities());
    }

    public List<SelectItem> getAcceptances() {
        return convertDictionaryItems(dictionariesService.getAcceptances());
    }

    public List<SelectItem> getOperatingSystems() {
        return convertDictionaryItems(dictionariesService.getOperatingSystems());
    }

    public List<SelectItem> getProjects() {
        return convertDictionaryItems(dictionariesService.getProjects());
    }

    public List<SelectItem> getProjectModules() {
        return convertDictionaryItems(dictionariesService.getProjectModules());
    }

    public List<SelectItem> getProjectVersions() {
        return convertDictionaryItems(dictionariesService.getProjectVersions());
    }

    public List<SelectItem> getBrowsers() {
        return convertDictionaryItems(dictionariesService.getBrowsers());
    }

    public List<SelectItem> getAreas() {
        return convertDictionaryItems(dictionariesService.getAreas());
    }

    public List<SelectItem> getRoles() {
        return convertRoles(Role.values());
    }

    public DictionaryName[] getDictionaryNames() {
        return DictionaryName.values();
    }

    private List<SelectItem> convertRoles(final Role[] roles) {
        final List<SelectItem> items = new ArrayList<SelectItem>();
        for (final Role role : roles) {
            items.add(new SelectItem(role, role.getNameToDisplay()));
        }
        return items;
    }

    private List<SelectItem> convertDictionaryItems(final Iterable<DictionaryEntry> entries) {
        final List<SelectItem> items = new ArrayList<SelectItem>();
        for (final DictionaryEntry entry : entries) {
            items.add(new SelectItem(entry.value, entry.valueToDisplay));
        }
        return items;
    }

    private List<SelectItem> convertUsers(final Iterable<User> users) {
        final List<SelectItem> items = new ArrayList<SelectItem>();
        for (final User user : users) {
            items.add(new SelectItem(user.getLogin(), user.getDisplayName()));
        }
        return items;
    }
}
