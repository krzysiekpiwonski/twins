package pl.piwonski.twins.messages;

import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.messages.domain.MessageSource;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author krzysiek@piwonski.pl
 */
@Named
@ApplicationScoped
public class MessageHelper {
    private final Map<String, String> messages = new HashMap<String, String>();

    public MessageHelper() {
        loadMessages(FacesContext.getCurrentInstance());
    }

    private void loadMessages(final FacesContext context) {
        for (final MessageSource messageSource : MessageSource.values()) {
            loadMessagesFormOneBundle(context, messageSource);
        }
    }

    private void loadMessagesFormOneBundle(final FacesContext context, final MessageSource messageSource) {
        final ResourceBundle resourceBundle = context.getApplication().getResourceBundle(context, messageSource.name());
        for (final String key : resourceBundle.keySet()) {
            messages.put(key, resourceBundle.getString(key));
        }
    }

    public String getMessage(final MessageKey key) {
        return messages.containsKey(key.name()) ? messages.get(key.name()) : key.name();
    }

    public void addMessageToPage(final MessageKey key) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(getMessage(key)));
    }
}
