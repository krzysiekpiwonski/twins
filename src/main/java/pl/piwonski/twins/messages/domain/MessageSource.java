package pl.piwonski.twins.messages.domain;

/**
 * @author krzysiek@piwonski.pl
 */
public enum MessageSource {
    CONFIG,
    TEXTS,
    URLS,
    VIEWS
}
