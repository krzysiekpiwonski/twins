package pl.piwonski.twins.questions.persist;

import pl.piwonski.twins.questions.domain.Question;

import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public interface QuestionDao {
    void saveQuestion(Question question);

    void deleteQuestion(long id);

    Question getQuestion(long id);

    List<Question> getQuestions();
}
