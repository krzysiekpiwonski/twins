package pl.piwonski.twins.questions.persist.binaryfile;

import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.common.persist.binaryfile.FileManager;
import pl.piwonski.twins.questions.domain.Question;
import pl.piwonski.twins.questions.persist.QuestionDao;
import pl.piwonski.twins.questions.persist.comparator.CompareQuestionsByAddDate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krzysiek@piwonski.pl
 */
class DefaultQuestionDao implements QuestionDao {
    private static final Logger LOGGER = Logger.getLogger(DefaultQuestionDao.class.getSimpleName());
    private final FileManager fileManager;
    private final File file;

    DefaultQuestionDao(final FileManager fileManager, final File file) {
        this.fileManager = fileManager;
        this.file = file;
    }

    @Override
    public final void saveQuestion(final Question question) {
        try {
            final HashMap<Long, Question> questions = readQuestions();
            questions.put(question.getId(), question);
            fileManager.writeObject(file, questions);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when saving question:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public final void deleteQuestion(final long id) {
        try {
            final HashMap<Long, Question> questions = readQuestions();
            questions.remove(id);
            fileManager.writeObject(file, questions);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when removing question:", e);
            throw new DaoException(e);
        }
    }

    @Override
    public final Question getQuestion(final long id) {
        final Question question;
        try {
            question = readQuestions().get(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting question:", e);
            throw new DaoException(e);
        }
        return question;
    }

    @Override
    public final List<Question> getQuestions() {
        final List<Question> questions;
        try {
            questions = new ArrayList<Question>(readQuestions().values());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "ERROR when getting questions:", e);
            throw new DaoException(e);
        }
        Collections.sort(questions, new CompareQuestionsByAddDate());
        return questions;
    }

    @SuppressWarnings("unchecked")
    private HashMap<Long, Question> readQuestions() throws ClassNotFoundException, IOException {
        HashMap<Long, Question> questions = (HashMap<Long, Question>) fileManager.readObject(file);
        if (questions == null) {
            questions = new HashMap<Long, Question>();
        }
        return questions;
    }
}
