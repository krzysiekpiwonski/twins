package pl.piwonski.twins.questions.persist.comparator;

import pl.piwonski.twins.questions.domain.Question;

import java.io.Serializable;
import java.util.Comparator;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public final class CompareQuestionsByAddDate implements Comparator<Question>,Serializable {
    private static final long serialVersionUID = -1094394455953925525L;

    @Override
    public int compare(final Question o1, final Question o2) {
        return o2.getAddDate().compareTo(o1.getAddDate());
    }
}
