package pl.piwonski.twins.questions.domain;

import pl.piwonski.twins.common.IdSequence;
import pl.piwonski.twins.common.domain.validator.Id;
import pl.piwonski.twins.common.domain.validator.Login;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public class Question implements Serializable {
    private static final long serialVersionUID = -8110444316169635274L;
    private static final String TEXT_EMPTY_FIELD = "{empty.field}";

    @Id
    @NotNull(message = TEXT_EMPTY_FIELD)
    private long id;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String title;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String body;

    @Login
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String author;

    @Past
    @NotNull(message = TEXT_EMPTY_FIELD)
    private Date addDate;

    @NotNull(message = TEXT_EMPTY_FIELD)
    private List<Answer> answers;

    @Inject
    private IdSequence idSequence;

    public Question() {
        this.id = idSequence.getNextValue();
        answers = new ArrayList<Answer>();
        addDate = new Date();
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getAuthor() {
        return author;
    }

    public List<Answer> getAnswers() {
        return Collections.unmodifiableList(answers);
    }

    public Date getAddDate() {
        return (Date) addDate.clone();
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public void addAnswer(final Answer answer) {
        answers.add(answer);
    }

    public int getAnswersCount() {
        return answers.size();
    }
}
