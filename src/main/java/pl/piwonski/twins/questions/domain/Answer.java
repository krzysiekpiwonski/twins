package pl.piwonski.twins.questions.domain;

import pl.piwonski.twins.common.domain.validator.Login;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.Date;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public class Answer implements Serializable {
    private static final long serialVersionUID = 4168804798574604743L;
    private static final String TEXT_EMPTY_FIELD = "{empty.field}";

    @NotNull(message = TEXT_EMPTY_FIELD)
    private String body;

    @Past
    @NotNull(message = TEXT_EMPTY_FIELD)
    private Date addDate;

    @Login
    @NotNull(message = TEXT_EMPTY_FIELD)
    private String author;

    public Answer() {
        addDate = new Date();
    }

    public String getBody() {
        return body;
    }

    public String getAuthor() {
        return author;
    }

    public Date getAddDate() {
        return (Date) addDate.clone();
    }

    public void setBody(final String body) {
        this.body = body;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }
}
