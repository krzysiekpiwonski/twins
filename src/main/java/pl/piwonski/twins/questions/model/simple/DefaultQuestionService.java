package pl.piwonski.twins.questions.model.simple;

import pl.piwonski.twins.common.annotations.PreferredImplementation;
import pl.piwonski.twins.questions.domain.Question;
import pl.piwonski.twins.questions.model.QuestionService;
import pl.piwonski.twins.questions.persist.QuestionDao;

import javax.inject.Inject;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
public class DefaultQuestionService implements QuestionService {
    @Inject
    @PreferredImplementation
    private QuestionDao questionDao;

    @Override
    public void addQuestion(final Question question) {
        questionDao.saveQuestion(question);
    }

    @Override
    public void updateQuestion(final Question question) {
        questionDao.saveQuestion(question);
    }

    @Override
    public void deleteQuestion(final long id) {
        questionDao.deleteQuestion(id);
    }

    @Override
    public Question getQuestion(final long id) {
        return questionDao.getQuestion(id);
    }

    @Override
    public List<Question> getQuestions() {
        return questionDao.getQuestions();
    }
}
