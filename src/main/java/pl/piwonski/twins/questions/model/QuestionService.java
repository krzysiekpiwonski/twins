package pl.piwonski.twins.questions.model;

import pl.piwonski.twins.questions.domain.Question;

import java.util.List;

/**
 * @author krzysztof.piwonski@agora.pl
 */
public interface QuestionService {
    void addQuestion(Question question);

    void updateQuestion(Question question);

    void deleteQuestion(long id);

    Question getQuestion(long id);

    List<Question> getQuestions();
}
