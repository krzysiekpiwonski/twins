package pl.piwonski.twins.questions.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.questions.domain.Answer;
import pl.piwonski.twins.questions.domain.Question;
import pl.piwonski.twins.questions.model.QuestionService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@ManagedBean
@ViewScoped
public class ViewQuestion {
    @Inject
    private QuestionService questionService;
    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;
    private Question question;
    private Answer answer;

    public Question getQuestion() {
        if (question == null) {
            question = questionService.getQuestion(requestParameters.getLongValue("id"));
        }
        return question;
    }

    public Answer getAnswer() {
        if (answer == null) {
            answer = new Answer();
        }
        return answer;
    }

    public String save() {
        String destination;
        try {
            question.addAnswer(answer);
            questionService.updateQuestion(question);
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_EDIT);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_NOT_SAVED);
        }
        return destination;
    }
}
