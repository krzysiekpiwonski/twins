package pl.piwonski.twins.questions.controller;

import pl.piwonski.twins.common.RequestParameters;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.questions.model.QuestionService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class DeleteQuestion {
    @Inject
    private QuestionService questionService;

    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private RequestParameters requestParameters;

    public String delete() {
        String destination;
        try {
            questionService.deleteQuestion(requestParameters.getLongValue("id"));
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_DELETED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_SHOW);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_NOT_DELETED);
        }
        return destination;
    }
}
