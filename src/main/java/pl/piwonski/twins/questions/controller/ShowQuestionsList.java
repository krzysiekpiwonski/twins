package pl.piwonski.twins.questions.controller;

import pl.piwonski.twins.questions.domain.Question;
import pl.piwonski.twins.questions.model.QuestionService;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.util.List;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class ShowQuestionsList {
    @Inject
    private QuestionService questionService;

    public List<Question> getQuestions() {
        return questionService.getQuestions();
    }
}
