package pl.piwonski.twins.questions.controller;

import pl.piwonski.twins.common.IdSequence;
import pl.piwonski.twins.messages.MessageHelper;
import pl.piwonski.twins.messages.domain.MessageKey;
import pl.piwonski.twins.common.persist.DaoException;
import pl.piwonski.twins.questions.domain.Question;
import pl.piwonski.twins.questions.model.QuestionService;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author krzysiek@piwonski.pl
 */
@Model
public class AddQuestion {
    @Inject
    private QuestionService questionService;
    @Inject
    private MessageHelper messagesHelper;
    @Inject
    private IdSequence idSequence;

    @Named
    @Produces
    private final Question question = new Question();

    public String add() {
        String destination;
        try {
            questionService.addQuestion(question);
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_INDEX);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_SAVED);
        } catch (final DaoException e) {
            destination = messagesHelper.getMessage(MessageKey.VIEW_QUESTIONS_ADD);
            messagesHelper.addMessageToPage(MessageKey.TEXT_QUESTIONS_NOT_SAVED);
        }
        return destination;
    }
}
