function confirmDelete() {
    return confirm("Na pewno usunąć?");
}

function showFields(elementId) {
    var element = document.getElementById(elementId);
    replaceClassName(element.getElementsByClassName('field'), 'hidden', 'showed');
    replaceClassName(element.getElementsByClassName('text'), 'showed', 'hidden');
}

function showTexts(elementId) {
    var element = document.getElementById(elementId);
    replaceClassName(element.getElementsByClassName('field'), 'showed', 'hidden');
    replaceClassName(element.getElementsByClassName('text'), 'hidden', 'showed');
}

function replaceClassName(elements, pattern, replacement) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].className = elements[i].className.replace(pattern, replacement);
    }
}
